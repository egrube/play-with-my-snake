#!/usr/bin/env node

import { SnakeGame } from "./src/game.js";
import { UI } from "./src/user-interface.js";

const game = new SnakeGame(new UI());
game.start();
